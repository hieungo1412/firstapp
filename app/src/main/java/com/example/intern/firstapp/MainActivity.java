package com.example.intern.firstapp;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText edtNum1;
    private EditText edtNum2;
    private EditText edtResult;
    private Button btnCal;
    private RadioGroup rdg;
    private RadioButton rdPlus;
    private RadioButton rdMinus;
    private RadioButton rdMulti;
    private RadioButton rdDivide;
    private int idChoose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        rdg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                idChoose = checkedId;
            }
        });

        btnCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double num1 = Double.parseDouble(edtNum1.getText().toString());
                double num2 = Double.parseDouble(edtNum2.getText().toString());
                switch (idChoose) {
                    case R.id.radioPlus:
                        edtResult.setText(String.valueOf(num1 + num2));

                        break;
                    case R.id.radioMinus:
                        edtResult.setText(String.valueOf(num1 - num2));

                        break;
                    case R.id.radioMulti:
                        edtResult.setText(String.valueOf(num1 * num2));

                        break;
                    case R.id.radioDivide:
                        if (num2 == 0) {
                            Toast msg = Toast.makeText(MainActivity.this, "The denominator must be different from zero", Toast.LENGTH_LONG);
                            msg.show();
                            edtNum2.setText(null);
                        } else {
                            edtResult.setText(String.valueOf(num1 / num2));
                        }
                        break;
                }
            }
        });
    }

    public void init() {
        edtNum1 = (EditText) findViewById(R.id.editText);
        edtNum2 = (EditText) findViewById(R.id.editText2);
        edtResult = (EditText) findViewById(R.id.editText3);
        btnCal = (Button) findViewById(R.id.buttonResult);
        rdg = (RadioGroup) findViewById(R.id.radioGroup);
        rdPlus = (RadioButton) findViewById(R.id.radioPlus);
        rdMinus = (RadioButton) findViewById(R.id.radioMinus);
        rdMulti = (RadioButton) findViewById(R.id.radioMulti);
        rdDivide = (RadioButton) findViewById(R.id.radioDivide);
    }


}
